[![pipeline](https://gitlab.com/nubex/panel/php-fpm-dev/badges/master/pipeline.svg)](https://gitlab.com/nubex/panel/php-fpm-dev/pipelines)

### Назначение
Этот образ используется в проекте [Панель Нубекса](https://gitlab.com/nubex/nubex-panel).

Образ предназначен для запуска в качестве сервиса описанного в docker-compose

Источником образа является [проект](https://gitlab.com/nubex/panel/php-fpm).



